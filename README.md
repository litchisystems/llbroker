# LLBroker (LitchiLyrics Broker)

A mosquitto broker with a user management.

## TODO:

- Use https://hub.docker.com/r/iegomez/mosquitto-go-auth docker image as base
- Build a mqtt-go-backend or a client with shared (sqlite) db, to allow an admin to:
- Add/Remove users
- Allowing users to change their password (or not, if users are used as groups)
- Changing permissions (define a clear mqtt permission api)
- Adding rooms (or allow private rooms)
- Clients should be able to subscribe to their permissions, to enable/disable features

## Requirements:
- It should be possible to add third-party services
- It should be possible to have multiple prefixes? (Multiple llbroker instances)
- It should be possible to add custom acls
- It should be possible to use not self hosted mqtt brokers
  (llbroker service should print out acls for manual configuration)

## Idea 1:
Every service publishes a set of permission levels a user can choose:
db/v1/permissions
read:
topic ...
write:
topic ...

rooms/v1/+/permissions
read:
topic ...
read+playlist:
topic ...
write:
topic ...

user/v1/permission/USERXYZ/db/v1: read
user/v1/permission/USERXYZ/rooms/v1/ROOMXYZ: read+playlist
